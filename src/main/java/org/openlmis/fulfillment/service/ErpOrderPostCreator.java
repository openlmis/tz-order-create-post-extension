/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.fulfillment.service;

import static org.openlmis.fulfillment.domain.OrderStatus.TRANSFER_FAILED;
import static org.openlmis.fulfillment.i18n.MessageKeys.ERROR_IO;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import lombok.RequiredArgsConstructor;
import org.openlmis.fulfillment.domain.FileTemplate;
import org.openlmis.fulfillment.domain.FtpTransferProperties;
import org.openlmis.fulfillment.domain.Order;
import org.openlmis.fulfillment.domain.OrderStatus;
import org.openlmis.fulfillment.domain.TransferProperties;
import org.openlmis.fulfillment.domain.TransferType;
import org.openlmis.fulfillment.extension.point.OrderCreatePostProcessor;
import org.openlmis.fulfillment.repository.OrderRepository;
import org.openlmis.fulfillment.repository.TransferPropertiesRepository;
import org.openlmis.fulfillment.service.referencedata.OrderableReferenceDataService;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component(value = "ErpOrderPostCreator")
@Primary
@RequiredArgsConstructor
public class ErpOrderPostCreator implements OrderCreatePostProcessor {
  private static final XLogger XLOGGER = XLoggerFactory.getXLogger(ErpOrderPostCreator.class);

  @Autowired
  private TransferPropertiesRepository transferPropertiesRepository;

  @Autowired
  private OrderStorage orderStorage;

  @Autowired
  private OrderSender orderSender;

  @Autowired
  private TzOrderCsvHelper csvHelper;

  @Autowired
  private FileTemplateService fileTemplateService;

  @Autowired
  private OrderRepository orderRepository;

  @Autowired
  private OrderableReferenceDataService orderableReferenceDataService;

  @Override
  public void process(Order order) {

    XLOGGER.entry(order);
    Profiler profiler = new Profiler("ERP_ORDER_CREATE_POST_PROCESSOR");
    profiler.setLogger(XLOGGER);

    XLOGGER.info("Order is an external, add columns and send to ERP system");

    order.setStatus(OrderStatus.ORDERED);

    order.getExtraData().put("externallyFulfilled", "true");

    XLOGGER.info("save Extra data");
    orderRepository.save(order);

    XLOGGER.debug("extra data = {}", order.getExtraData());

    TransferProperties properties = transferPropertiesRepository
        .findFirstByFacilityIdAndTransferType(order.getSupplyingFacilityId(),
            TransferType.ORDER);

    if (null == properties) {
      XLOGGER.warn(
          "Can't store the order {} because there is no transfer properties",
          order.getId()
      );
      return;
    }

    if (properties instanceof FtpTransferProperties) {
      XLOGGER.debug("Export file and try to send to FTP server");

      FileTemplate fileTemplate
          = fileTemplateService.getOrderFileTemplate();
      // retrieve order file template
      String fileName = fileTemplate.getFilePrefix() + order.getOrderCode() + ".csv";
      Path path;

      // retrieve order file template

      try {
        String dir = properties.getPath();
        Files.createDirectories(Paths.get(dir));
        path = Paths.get(dir, fileName);
      } catch (IOException exp) {
        throw new OrderStorageException(exp, ERROR_IO, exp.getMessage());
      }

      try (Writer writer = Files.newBufferedWriter(path)) {
        // 1. generate CSV file using order file template
        // 2. save generated CSV file in local directory

        csvHelper.writeCsvFile(order, fileTemplate, writer);
      } catch (IOException exp) {
        throw new OrderStorageException(exp, ERROR_IO, exp.getMessage());
      }

      boolean success = orderSender.send(order);

      if (success) {
        orderStorage.delete(order);
      } else {
        order.setStatus(TRANSFER_FAILED);
      }

    }

  }

}
