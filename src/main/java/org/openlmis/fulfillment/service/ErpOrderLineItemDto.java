/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2021 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

package org.openlmis.fulfillment.service;

import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.fulfillment.domain.Order;
import org.openlmis.fulfillment.domain.OrderLineItem;
import org.openlmis.fulfillment.service.referencedata.FacilityDto;
import org.openlmis.fulfillment.service.referencedata.OrderableDto;
import org.openlmis.fulfillment.service.referencedata.ProcessingPeriodDto;

@NoArgsConstructor
@AllArgsConstructor
public class ErpOrderLineItemDto {

  private static final String COMPANY_FIELD_VALUE = "MSD-FIN";
  private static final String CURRENCY_FIELD_VALUE = "TZS";
  private static final String UOM_FIELD = "uom";
  private static final String PRODUCT_CODE_FIELD = "productCode";
  private static final String REVISION_FIELD = "revision";

  @Setter
  @Getter
  private String plant;

  @Setter
  @Getter
  private String company;

  @Setter
  @Getter
  private String facilityId;

  @Setter
  @Getter
  private String revision;

  @Setter
  @Getter
  private String uom;

  @Setter
  @Getter
  private String currency;

  @Setter
  @Getter
  private String period;

  @Setter
  @Getter
  private String quoteDate;

  @Setter
  @Getter
  private String description;

  @Setter
  @Getter
  private String price;

  @Setter
  @Getter
  private String error;

  @Setter
  @Getter
  private String quoteNumber;

  @Setter
  @Getter
  private String orderLine;

  @Setter
  @Getter
  private String status;

  @Setter
  @Getter
  private String priceListCode;

  @Setter
  @Getter
  private String quoteLine;

  @Setter
  @Getter
  private String supplyingFacilityName;

  @Setter
  @Getter
  private boolean externallyFulfilled;

  @Setter
  @Getter
  private String customerId;

  @Setter
  @Getter
  private String productCode;

  @Setter
  @Getter
  private String orderNumber;

  @Setter
  @Getter
  private String approvedQuantity;

  /**
   * Create new ErpOrderLineItemDto object based on object of {@link Order}.
   * @param order Object of order
   * @param orderLineItem Object of order line item
   * @param orderablesMap Object of orderable line
   * @param facilityDto Object of Facilities
   * @param supplyingFacility Object of Facility
   * @param processingPeriodDto Object of processing period
   * @param counter number of order line items
   */
  public ErpOrderLineItemDto(Order order, OrderLineItem orderLineItem,
                             Map<UUID, OrderableDto> orderablesMap, FacilityDto facilityDto,
                             FacilityDto supplyingFacility,
                             ProcessingPeriodDto processingPeriodDto, int counter) {
    this.plant = supplyingFacility.getCode();
    this.company = COMPANY_FIELD_VALUE;
    this.orderNumber = order.getOrderCode();
    String extraDataProductCode = orderablesMap
        .get(orderLineItem.getOrderable().getId()).getExtraData()
        .getOrDefault(PRODUCT_CODE_FIELD, null);

    if (extraDataProductCode != null) {
      this.productCode = extraDataProductCode;
      this.uom = orderablesMap
          .get(orderLineItem.getOrderable().getId()).getExtraData()
          .getOrDefault(UOM_FIELD, null);
      this.revision = orderablesMap
          .get(orderLineItem.getOrderable().getId()).getExtraData()
          .getOrDefault(REVISION_FIELD, null);
    } else {
      this.productCode = orderablesMap
          .get(orderLineItem.getOrderable().getId()).getProductCode();
    }
    this.customerId = facilityDto.getCode();
    this.currency = CURRENCY_FIELD_VALUE;
    this.approvedQuantity = orderLineItem.getOrderedQuantity().toString();
    this.period = processingPeriodDto.getStartDate()
        .format(DateTimeFormatter.ISO_LOCAL_DATE);
    this.orderLine = String.valueOf(counter);
    this.status = order.getStatus().toString();

  }

}
